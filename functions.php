<?php

// Include the HTML DOM parser.
include_once("simple_html_dom.php");

// Create DOM from URL or file
$html = file_get_html('http://regweb.kfupm.edu.sa/Student/course_open1.asp');

// Get the current term form.
$currentTerm = getCurrentTermTD($html, "141");
$currentTermForm = getCurrentTermForm($currentTerm);


// Return the current term td element which contains the form and the submit button.
function getCurrentTermTD($html, $term)
{
	// Find all forms.
	foreach($html->find('form[name=prod_sel]') as $element)
	{
		// Find the p elements from the form element.
		$pElement = $element->parent()->parent()->parent()->find('p');

		foreach($pElement as $p)
		{
			if(strpos($p->plaintext, $term) !== false)
			{
				// Return the td element from the paragraph element using the parent function.
				return $element->parent()->parent()->parent()->parent()->parent()->parent();
			}
		}
	}
} //END OF getCurrentTermTD FUNCTION.

// Return the form from a td element.
function getCurrentTermForm($td)
{
	return $td->find('form', 0);
} //END OF getCurrentTermForm FUNCTION.

// Get the course page.
function getCourseHTML($url, $course)
{

	// Generate URL-encoded query string that contains the data 'course'.
	$postdata = http_build_query(
			array(
					'course' => $course,
					'B1' => 'Submit'
			)
	);

	// Specify the request options.
	$opts = array('http' =>
			array(
					'method'  => 'POST',
					'header'  => 'Content-type: application/x-www-form-urlencoded',
					'content' => $postdata
			)
	);

	// Create the stream context for the options.
	$context = stream_context_create($opts);

	return file_get_html('http://regweb.kfupm.edu.sa/Student/course_open4.asp'/*REPLACE WITH $url*/, false, $context);
}

// Return the 
function getMajorCourses($major)
{
	
	// Retrieve the major html page from the reg web.
	$majorHTML = getCourseHTML("", $major);
	
	// Store all the sections of $major, it includes arrays '$rowArray'.
	$majorArray = array();
	
	// Get the information row in the section table.
	$informationRowElement = $majorHTML->find('table', 1)->find('tr', 0);
	
	// Get the sections table.
	$sectionsTableElement = $majorHTML->find('table', 1);

	// Store the information cells in an array.
	$informationRowArray = array();
	
	foreach ($informationRowElement->find('td') as $info)
	{
		array_push($informationRowArray, str_replace("&nbsp;","",rtrim(ltrim($info->plaintext))));
	}
	
	// Skip the first row.
	$firstTR = true;
	
	foreach($sectionsTableElement->find('tr') as $row) {
		$rowArray = array();
		$i = 0;
		
		if(!$firstTR)
		{
			foreach ($row->find('td') as $cell) {
				$rowArray[$informationRowArray[$i]] = str_replace("&nbsp;","",rtrim(ltrim($cell->plaintext)));
				$i++;
			}
			array_push($majorArray, $rowArray);
		}
		$firstTR = false;
	}	
	//print_r($majorArray);
	return $majorArray;
}

// Return index of $string occurance in array.
function getIndex($array, $string)
{
	$index = 0;
	foreach($array as $var)
	{
		if(strpos($var,$string) == true)
		{
			break;
		}
		$index++;
	}

	return $index;
}

function getCRNStatus($crn, $crnInfoIndex, $statusInfoIndex, $sectionsTableElement)
{
	$crnArray = array();
	foreach($sectionsTableElement->find('tr') as $tr)
	{
		array_push($crnArray, $tr->find('td', $crnInfoIndex));
	}

	// Get the section index from the list of sections rows list.
	$sectionIndex = getIndex($crnArray, $crn);
	
	// Get the section row with the specified crn.
	$sectionRow = $sectionsTableElement->find('tr', $sectionIndex);
	
	return $sectionRow->find('td', $statusInfoIndex)->plaintext;
}
?>