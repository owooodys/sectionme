<?php

include('dbConnection.php');
include 'functions.php';

//include_once 'DBAbstractionLayer.php';

// Get the CRN Course and its status after an ajax.
if(isset($_POST["crn"]))
{
	$crn = $_POST["crn"];
	$query = mysqli_query($con, "SELECT Dep FROM Section WHERE CRN = '$crn'");
	$row = mysqli_fetch_assoc($query);
	
	// Send a request to the reg web to retrieve the course page with its sections.
	$courseHTML = getCourseHTML("", $row['Dep']);

	// Get the information row in the section table.
	$informationRowElement = $courseHTML->find('table', 1)->find('tr', 0);

	// Array to store information row columns.
	$informationRowArray = array();

	// Apend the information into an array.
	foreach($informationRowElement->find('td') as $info)
	{
		array_push($informationRowArray, $info->plaintext);
	}

	// Get the sections table.
	$sectionsTableElement = $courseHTML->find('table', 1);

	// Get the index of the CRN info column in the information raw.
	$crnInfoIndex = getIndex($informationRowArray, "CRN");

	// Get the index of the Status info column in the information raw.
	$statusInfoIndex = getIndex($informationRowArray, "Status");

	// Get the index of the course name info column in the information raw.
	$courseInfoIndex = getIndex($informationRowArray, "Course");
	
	// Get the course status and course name from the course row.
	$courseStatus = getCRNStatus($_POST["crn"], $crnInfoIndex, $statusInfoIndex, $sectionsTableElement);
	$courseName = getCRNStatus($_POST["crn"], $crnInfoIndex, $courseInfoIndex, $sectionsTableElement);
		
	// Build the json.
	$arr = array('status' => $courseStatus, 'course' => $courseName);
	$arr["json"] = json_encode($arr);
	echo json_encode($arr);
}

function getMajor($con, $crn) {
	
}
?>