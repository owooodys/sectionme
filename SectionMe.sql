-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 12, 2014 at 07:35 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `SectionMe`
--

-- --------------------------------------------------------

--
-- Table structure for table `Section`
--

CREATE TABLE IF NOT EXISTS `Section` (
  `term` int(3) NOT NULL,
  `Dep` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Course` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  `SECNO` int(3) NOT NULL,
  `CRN` int(5) NOT NULL,
  `CourseName` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(3) COLLATE utf8_unicode_ci NOT NULL COMMENT 'LEC, LAB,COP',
  `instructor` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `day` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `StartT` int(4) NOT NULL,
  `EndT` int(4) NOT NULL,
  `BLD` int(2) DEFAULT NULL,
  `Room` int(10) DEFAULT NULL,
  PRIMARY KEY (`CRN`),
  UNIQUE KEY `CRN` (`CRN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `SUBMISSIONS`
--

CREATE TABLE IF NOT EXISTS `SUBMISSIONS` (
  `SubTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `mobile` int(9) NOT NULL,
  `CRN` int(5) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `mobile` (`mobile`),
  UNIQUE KEY `CRN` (`CRN`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=233 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `SUBMISSIONS`
--
ALTER TABLE `SUBMISSIONS`
  ADD CONSTRAINT `FK1` FOREIGN KEY (`CRN`) REFERENCES `SECTION` (`CRN`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
