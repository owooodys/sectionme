<?php 

// Display errors.
error_reporting(E_ALL);
ini_set('display_errors', 1);

include_once 'functions.php';

?>


<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="Resources/css/bootstrap.css">
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700|Droid+Serif' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="Resources/css/SectionMe.css">
	<link rel="stylesheet" href="Resources/css/Timeline.css">
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	<script src="Resources/js/modernizr.js"></script>	
	<script src="Resources/js/jquery-1.11.1.js"></script>
	<script src="Resources/js/SectionMe.js"></script>
</head>
<body>
    <main>
		<header class="cd-section">
			<div class="cd-container">
				<div class="row">
					<!-- <div class="col-md-offset-4"><i class="fa fa-life-ring fa-5x"></i></div> -->
						<h1>Section Me</h1>
						<p>Never lose your favourite section!</p>
				</div>
			</div> <!-- cd-container -->
		</header>
		
		<section class="cd-section cd-placeholder-2">
			<div class="cd-container">
				<div class="row">
					<ul>
						<div class="col-md-3">
							<li class="cd-single-point">
								<a class="cd-img-replace" href="#0"></a>
								<div class="cd-more-info cd-top">
									<h2>Choose Course</h2>
									<p>Help us know what major are you searching for?</p>
									<a href="#0" class="cd-close-info cd-img-replace">Close</a>
								</div>
							</li> <!-- .cd-single-point -->
						</div>
						<div class="col-md-5">
							<li class="cd-single-point">
								<a class="cd-img-replace" href="#0"></a>
								<div class="cd-more-info cd-top">
									<h2>Enter crn</h2>
									<p>Enter the crn of the section you want to register</p>
									<a href="#0" class="cd-close-info cd-img-replace">Close</a>
								</div>
							</li> <!-- .cd-single-point -->
						</div>
						<div class="col-md-4">
							<li class="cd-single-point">
								<a class="cd-img-replace" href="#0"></a>
								<div class="cd-more-info cd-right">
									<h2>Confirm</h2>
									<p>We will Section You once the section opens :)</p>
									<a href="#0" class="cd-close-info cd-img-replace">Close</a>
								</div>
							</li> <!-- .cd-single-point -->
						</div>
					</ul>
				</div>
			
				<div class="row">
					<div style="margin-top: 200px">						
						<div class="col-md-8">
							<form action="getCrnCourse.php" method="post" class="cd-form">
								<div class="col-md-5">
									<select class="select-styled" name="course">
										<option>Choose a course..</option>
										<?php 
										foreach($currentTermForm->find('option') as $course)
										{
											echo "<option>$course->plaintext</option>";
										}
										//echo $currentTermForm 
										?>
									</select>
								</div>
									
								<div class="col-md-7" style="height: 100px;">
									<label class="cd-label" for="cd-email">crn</label>
									<input type="text" id="cd-email" class="cd-email" name="crn" placeholder="Enter your section crn">
									<input type="submit" class="cd-submit" value="Submit">
									<div class="cd-loading"></div>
									<div data-type="message" class="cd-response cd-response-error">Ops! This section is OPEN, hurry up and register it..</div>
									<div data-type="message" class="cd-response-success"><p>Great! We found your CLOSED section..</p></div>
									<div data-type="message" class="cd-response cd-response-notification">Invalid crn, please try again :)</div>
								</div>
							</form>
						</div>
						<div class="col-md-4">
							<div id="course">
								<p>test</p>
							</div>
						</div>
					</div>
				</div>
			
			</div> <!-- cd-container -->
		</section>

		<section id="cd-team" class="cd-section">
			<div class="cd-container">
				<h2>Our team</h2>
				<ul>
					<li>
						<a href="#0" data-type="member-1">
							<figure>
								<img src="img/team-member-1.jpg" alt="Team member 1">
								<div class="cd-img-overlay"><span>View Bio</span></div>
							</figure>

							<div class="cd-member-info">
								Osama Al-Najjar <span>SWE Student</span>
							</div> <!-- cd-member-info -->
						</a>
					</li>
					
					<li>
						<a href="#0" data-type="member-2">
							<figure>
								<img src="img/team-member-1.jpg" alt="Team member 1">
								<div class="cd-img-overlay"><span>View Bio</span></div>
							</figure>

							<div class="cd-member-info">
								Mohammad Bagabas <span>Industrial Student</span>
							</div> <!-- cd-member-info -->
						</a>
					</li>

					<li>
						<a href="#0" data-type="member-3">
							<figure>
								<i style="color: white;" class="fa fa-heart fa-5x"></i>
								<div class="cd-img-overlay"><span>Contact Us</span></div>
							</figure>

							<div class="cd-member-info">
								You! <span>Feedback</span>
							</div> <!-- cd-member-info -->
						</a>
					</li>
				</ul>
			</div> <!-- cd-container -->
		</section> <!-- cd-team -->

		<div class="cd-overlay"></div>
	</main>

	<div class="cd-member-bio member-1">
		<div class="cd-member-bio-pict">
			<img src="img/member-bio-img-1.jpg" alt="Member Bio image">
		</div> <!-- cd-member-bio-pict -->

		<div class="cd-bio-content">
			<h1>Meet Osama Al-Najjar</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non, amet, voluptatibus et omnis dolore illo saepe voluptatem qui quibusdam sunt corporis ut iure repellendus delectus voluptate explicabo temporibus quos eaque?</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, explicabo, doloribus, esse rem officia quas facilis eius alias similique ducimus amet quam odio perspiciatis dolorem ipsam! Ab, dolores, adipisci, explicabo pariatur illum deleniti quam iusto placeat nisi aliquam praesentium mollitia eligendi a! Sequi, voluptatibus sed quos possimus harum rem tempore fugiat? Corporis, officia, assumenda, asperiores blanditiis quidem aliquam fugiat vel excepturi velit provident aut omnis quos aliquid tempora eaque. Nemo, eveniet, sint maxime eum maiores totam est inventore numquam voluptatem hic nam blanditiis placeat illum nesciunt voluptatum eos cum quos magni voluptates ipsam. Perspiciatis alias ducimus libero. Quo provident omnis fugiat ut repellendus optio cum quaerat odio et ipsa. Molestias, atque repellat non maxime amet corporis magni libero quam numquam error beatae at asperiores et a porro nostrum ab necessitatibus esse aliquid iure repellendus obcaecati unde quo eius eum dolores quasi consectetur culpa velit optio! Sequi, dolor, minima, veniam doloribus in ullam cupiditate iste animi ipsum esse eaque similique illo temporibus magni et earum amet sint deleniti est reiciendis. Maxime, quis, animi, ad quasi adipisci suscipit alias iste reprehenderit ea placeat nulla commodi nobis magnam provident veniam earum odit eveniet possimus aut voluptatum dolorum culpa necessitatibus facere totam quisquam. Ipsam.</p>
		</div> <!-- cd-bio-content -->
	</div> <!-- cd-member-bio -->

	<div class="cd-member-bio member-2">
		<div class="cd-member-bio-pict">
			<img src="img/member-bio-img-2.jpg" alt="Member Bio image">
		</div> <!-- cd-member-bio-pict -->

		<div class="cd-bio-content">
			<h1>Meet Cristina Cameron</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non, amet, voluptatibus et omnis dolore illo saepe voluptatem qui quibusdam sunt corporis ut iure repellendus delectus voluptate explicabo temporibus quos eaque?</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, explicabo, doloribus, esse rem officia quas facilis eius alias similique ducimus amet quam odio perspiciatis dolorem ipsam! Ab, dolores, adipisci, explicabo pariatur illum deleniti quam iusto placeat nisi aliquam praesentium mollitia eligendi a! Sequi, voluptatibus sed quos possimus harum rem tempore fugiat? Corporis, officia, assumenda, asperiores blanditiis quidem aliquam fugiat vel excepturi velit provident aut omnis quos aliquid tempora eaque. Nemo, eveniet, sint maxime eum maiores totam est inventore numquam voluptatem hic nam blanditiis placeat illum nesciunt voluptatum eos cum quos magni voluptates ipsam. Perspiciatis alias ducimus libero. Quo provident omnis fugiat ut repellendus optio cum quaerat odio et ipsa. Molestias, atque repellat non maxime amet corporis magni libero quam numquam error beatae at asperiores et a porro nostrum ab necessitatibus esse aliquid iure repellendus obcaecati unde quo eius eum dolores quasi consectetur culpa velit optio! Sequi, dolor, minima, veniam doloribus in ullam cupiditate iste animi ipsum esse eaque similique illo temporibus magni et earum amet sint deleniti est reiciendis. Maxime, quis, animi, ad quasi adipisci suscipit alias iste reprehenderit ea placeat nulla commodi nobis magnam provident veniam earum odit eveniet possimus aut voluptatum dolorum culpa necessitatibus facere totam quisquam. Ipsam.</p>
		</div> <!-- cd-bio-content -->
	</div> <!-- cd-member-bio -->

	<div class="cd-member-bio member-3">
		<div class="cd-bio-content">
			<h1>We are here for you 24/7</h1>
			<p>Feel free to email us to provide some feedback, report a problem!<br>Or to just say hello :)</p>
		</div> <!-- cd-bio-content -->
	</div> <!-- cd-member-bio -->
	<a href="#0" class="cd-member-bio-close">Close</a>
</body>


<script type="text/javascript">
$("document").ready(function(){
	$(".cd-form").submit(function(){
    	var data = {
      		"action": "test"
    	};
    	data = $(this).serialize() + "&" + $.param(data);
    	$.ajax({
      		type: "POST",
      		dataType: "json",
      		url: "getCrnCourse.php", //Relative or absolute path to response.php file
      		data: data,
      		beforeSend:function(){
      			$('.cd-form').addClass('is-submitted').find('.cd-loading').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend');
			},
      		success: function(data) {
    	  		$('.cd-form').removeClass('is-submitted').find('.cd-loading').off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend');
          		var courseStatus = data["status"];
          		var courseName = data["course"];

          		if(courseStatus.indexOf("CLOSE") >= 0 )
          		{
					// Save course name and other stuff to hidden id input.
        	  		$('.cd-response-success').addClass('slide-in');
        	  		$('#course').html(courseName);
          		} else {
        	  		$('.cd-response-error').addClass('is-visible');
          		}
          		//OPEN    &nbsp;
          		//alert(courseStatus);
          		//alert(courseName); fghtnroigndfljnfoubn
      		},
      		error: function() {
      			$('.cd-form').removeClass('is-submitted').find('.cd-loading').off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend');
    	  		$('.cd-response-notification').addClass('is-visible');
      		}
    	});
    	return false;
	});
});
</script>

</html>