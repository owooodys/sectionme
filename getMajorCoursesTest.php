<?php

// Include the HTML DOM parser.
include_once("simple_html_dom.php");
include_once 'functions.php';


// Create DOM from URL or file
$html = file_get_html('http://regweb.kfupm.edu.sa/Student/course_open1.asp');

// Get the current term form.
$currentTerm = getCurrentTermTD($html, "141");
$currentTermForm = getCurrentTermForm($currentTerm);

if(isset($_POST["course"]))
{
	getMajorCourses($_POST["course"]);
}

?>

<html>
<body>

<form action="getMajorCoursesTest.php" method="post" class="cd-form">
	<div class="col-md-5">
		<select class="select-styled" name="course">
			<option>Choose a course..</option>
			<?php 
			foreach($currentTermForm->find('option') as $course)
			{
				echo "<option>$course->plaintext</option>";
			}
			//echo $currentTermForm 
			?>
		</select>
		<button type="submit">Submit</button>
	</div>
</form>

</body>

</html>