(function optimOverTracking ($) {
	var trackDataChange = setInterval(function () {
		var $overlayElm = $('.zen-overlay-chrome');

		if (typeof $overlayElm.attr('data-status') === "undefined" ||  $overlayElm.attr('data-status') === "" || $.isEmptyObject($overlayElm.data()) ) {
			return;
		}

		//What is the status
		var status = $overlayElm.attr('data-status');
		if(status === "success"){
			pushOptimEvent('add');
		}else if (status === "failure") {
			pushOptimEvent('cancel');
          	
          	_kiq.push(['set', {  
                'chrome_cancel': true
            }]);
		}

	}, 500);

	function pushOptimEvent (status) {
		//Initialize Optimizely
		window['optimizely'] = window['optimizely'] || [];
      
      	try {
        	ga('send', 'pageview', '/abtest/' + optimizely.activeExperiments + '/' + optimizely.variationMap[optimizely.activeExperiments + ''] + '/' + status);
        }
      	catch(e) {
        	
        }
      	
		//Push FAIL event to optimizely
		window.optimizely.push(["trackEvent", "chrome_" + status]);
		clearInterval(trackDataChange);
	}
})(jQuery);
