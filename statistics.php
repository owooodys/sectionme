<?php

function getUserRequestsCount($con){
	$result = mysqli_query($con, 'SELECT COUNT(CRN) FROM submissions');
	return $result;
}

function getSystemRequestsCount($con){
	//$result = mysqli_query();
}

function getMajorCount($con){
	$result = mysqli_query($con,'SELECT COUNT(DISTINCT Dep) FROM section');
	return $result;
}

function getCourseCount($con){
	$result = mysqli_query($con,'SELECT COUNT(DISTINCT Course) FROM section');
	return $result;
}

function getCourseStartAtCount($con, $startTime){
	$result = mysqli_query($con,"SELECT COUNT(StartT) FROM section WHERE StartT = '".$startTime."'");
	return $result;
}

function getClassesInBuildingCount($con, $buildingNum){
	$result = mysqli_query($con,"SELECT COUNT(BLD) FROM section WHERE BLD = '".$buildingNum."'");
	return $result;
}

?>
