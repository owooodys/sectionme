
    <!doctype html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <title>SectionMe</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
            <link href="css/flexslider.min.css" rel="stylesheet" type="text/css" media="all"/>
            <link href="css/main.css" rel="stylesheet" type="text/css" media="all"/>
            <link href="css/elegant-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
            <link href="css/pe-icon-7-stroke.min.css" rel="stylesheet" type="text/css" media="all"/>
            <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all"/>
            <link href="css/theme-lava.css" rel="stylesheet" type="text/css" media="all"/>
            <link href="//d1dz47xko52i1p.cloudfront.net/assets/zen-cb444b537809c10d321874741cebd0d4.css" media="screen" rel="stylesheet"> 
            <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,300' rel='stylesheet' type='text/css'>
            <link href="css/css-cards.css" rel="stylesheet" type="text/css" media="all"/>
            
            <script src="Resources/js/modernizr.js"></script>	
            <script src="Resources/js/jquery-1.11.1.js"></script>
            <script src="Resources/js/ladda.js"></script>	
            <script src="Resources/js/spin.js"></script>
        </head>
        <body>

            <div class="loader">
                <div class="strip-holder">
                    <div class="strip-1"></div>
                    <div class="strip-2"></div>
                    <div class="strip-3"></div>
                </div>
            </div>

            <a id="top"></a>

            <div class="nav-container">

                    <nav class="overlay-nav">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-2">
                                        <img alt="Logo" class="logo logo-light" src="img/Black.png">
                                        <img alt="Logo" class="logo logo-dark" src="img/Black.png">
                                </div>

                                <div class="col-md-10 text-right">
                                    <ul class="menu" style="margin-right: 32px;">
                                        <li ><a class="inner-link" href="#home" target="default">home</a>
                                        </li>

                                        <li><a class="inner-link" href="#developers" target="default">Devolopers</a></li>
                                        </a>
                                    </ul>
                                    <div class="sidebar-menu-toggle"><i class="icon icon_menu"></i></div>
                                    <div class="mobile-menu-toggle"><i class="icon icon_menu"></i></div>
                                </div>
                            </div><!--end of row-->
                        </div><!--end of container-->

                        <div class="bottom-border"></div>

                        <div class="sidebar-menu">
                            <img alt="Logo" class="logo" src="img/white.png">
                            <div class="bottom-border"></div>
                            <div class="sidebar-content">

                                <div class="widget">
                                    <ul class="menu" style="margin-right: 32px;">
                                        <li><a class="inner-link" href="#home" target="default">home</a></li>

                                        <li><a class="inner-link" href="#developers" target="default">Develpoers</a></li>

                                    </ul>
                                </div>

                            </div><!--end of sidebar content-->
                        </div><!--end of sidebar-->

                    </nav>



            </div>


            <div class="main-container"><a id="home" class="in-page-link"></a>

                
                 <section class="subscribe-2"  id="CRN">

                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img alt="logo" class="logo" src="img/Black.png">
                                <p><br><br><br><br></p>
                                <h1 class="large-h1">Enter the CRN to check its availability:</h1>

                            </div>
                        </div><!--end of row-->

                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                                <form class="email-subscribe mail-list-signup">
                                    <div class="col-sm-7">
                                        <input name="crn" class="form-email signup-email-field" type="text" placeholder="Put your CRN">
                                    </div>

                                    <div class="col-sm-5">
                                        <input class="btn" data-style="expand-right" type="submit" value="Check">
                                    </div>
                                </form>
                            </div>
                        </div><!--end of row-->
                    </div><!--end of container-->

                </section>
                
                

                <section class="hero-slider"  id="timer">
                    <ul class="slides">
                        <li class="countdown-header primary-overlay">
                            <div class="background-image-holder parallax-background"></div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                                        <h1 class="text-black large-h1" > Time until next sections' lists update:</h1>
                                    </div>
                                </div><!--end of row-->
                                
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                                        <h1 id="countdown"></h1>
                                    </div>
                                </div>
                            </div><!--end of container-->
                        </li>
                    </ul>
                </section>

               

                <a id="about" class="in-page-link"></a>


                <a id="developers" class="in-page-link"></a>



                <section class="developers duplicatable-content" id="aboutUs">

                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12" >
                                <div id="developersyahoo">
                                </div>
                                <img class="background-image" alt="Developer" src="img/devs.png">
                            </div>
                        </div><!--end of row-->



                        <div class="zen-section">
                  <div class="row">
                    <div class="column">
                        <ul class="zen-polaroids large-block-grid-4 medium-block-grid-3 small-block-grid-1">

                            <li>
                              <article>
                                <header>
                                    <img class="background-image" alt="Developer" src="img/Aziz.jpg">
                                    Abdulaziz Alkhoraidly<br>
                                    <small>Consultant</small>
                                </header>
                                <footer>
                                    <h4>
                                        Abdulaziz Alkhoraidly
                                    </h4>
                             Dr. Alkhoraidly is Professor at KFUPM, CCSE, ICS Department. BOOORIINNG!! Dr. Aziz really helped creating a facinating environment full of cheer and laughter and solutions for our project.
                                     <br>
                                    <div class="image-holder">
                                        <br>
                                        <div class="hover-state text-center preserve3d">
                                            <div class="social-links ">
                                                <a href="#"><i class="icon social_twitter"></i></a>
                                                <a href="#"><i class="icon social_linkedin"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </footer>
                              </article>
                            </li> <!-- End of a developer-->


                            <li>
                              <article>
                                <header>
                                    <img class="background-image" alt="Developer" src="img/Bo%203azah.jpg">
                                    Abdulaziz Al-Zahrani<br>
                                    <small>Picasso's Eyes</small>
                                </header>
                                <footer>
                                    <h4>
                                        Abdulaziz Al-Zahrani
                                    </h4>
                                        He's the color for the flower, the light in the dark, the bla bla in the bla bla. He is a great photographer designer and one hell of a developer.
                                     <br>
                                    <div class="image-holder">
                                        <br>
                                        <div class="hover-state text-center preserve3d">
                                            <div class="social-links ">
                                                <a href="#"><i class="icon social_twitter"></i></a>
                                                <a href="#"><i class="icon social_linkedin"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </footer>
                              </article>
                            </li><!-- End of a developer-->



                            <li>
                              <article>
                                <header>
                                    <img class="background-image" alt="Developer" src="img/Bu-Saleh.jpg">
                                   Ali Bu-Saleh<br>
                                    <small>Pringles Man</small>
                                </header>
                                <footer>
                                    <h4>
                                        Ali Bu-Saleh
                                    </h4>
                                        BuSaleh is a one freakin' piece of work, he speaks really fast and masha' Allah thinks faster. He also did a lot of this website's magic.
                                     <br>
                                    <div class="image-holder">
                                        <br>
                                        <div class="hover-state text-center preserve3d">
                                            <div class="social-links ">
                                                <a href="#"><i class="icon social_twitter"></i></a>
                                                <a href="#"><i class="icon social_linkedin"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </footer>
                              </article>
                            </li>




                            <li>
                              <article>
                                <header>
                                    <img class="background-image" alt="Developer" src="img/Ghazi.jpg">
                                    Ghazi Al-Ghamdi<br>
                                    <small>Otacon</small>
                                </header>
                                <footer>
                                    <h4>
                                        Ghazi Al-Ghamdi
                                    </h4>
                            I can call Ghazi with The Gold Digger, I mean masha' Allah he brings tools from underground to use in order to help accomblishing our task.
                                     <br>
                                    <div class="image-holder">
                                        <br>
                                        <div class="hover-state text-center preserve3d">
                                            <div class="social-links ">
                                                <a href="#"><i class="icon social_twitter"></i></a>
                                                <a href="#"><i class="icon social_linkedin"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </footer>
                              </article>
                            </li>



                            <li>
                              <article>
                                <header>

                                    <img class="background-image" alt="Developer" src="img/Hoska.jpg">
                                    Hussam Gharawi<br>
                                    <small>Leonardo Davenci's Brain</small>
                                </header>
                                <footer>
                                    <h4>
                                        Hussam Gharawi
                                    </h4>
                             Hussam is crazy about UI/UX. He is so crazy that he rates every App, website and human being depending on his appearance.<br>
                                            (Not kidding man!). He also wrote the team's bio.
                                    <br>
                                    <div class="image-holder">
                                        <br>
                                        <div class="hover-state text-center preserve3d">
                                            <div class="social-links ">
                                                <a href="#"><i class="icon social_twitter"></i></a>
                                                <a href="#"><i class="icon social_linkedin"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </footer>
                              </article>
                            </li>



                            <li>
                              <article>
                                <header>
                                    <img class="background-image" alt="Developer" src="img/BaGabbas.JPG">
                                    Mohammed Ba-Gabas<br>
                                    <small>Ideas Factory</small>
                                </header>
                                <footer>
                                    <h4>
                                        Mohammed Ba-Gabas
                                    </h4>
                                        Ba-Gabas or whatever his name is, has a really great mind. He's so lazy that he created this Idea. NICE Idea man.
                                    <br>
                                    <br>
                                        <div class="hover-state text-center preserve3d">
                                            <div class="social-links ">
                                                <a href="#"><i class="icon social_twitter"></i></a>
                                                <a href="#"><i class="icon social_linkedin"></i></a>
                                            </div>
                                        </div>
                                </footer>
                              </article>
                            </li>




                            <li>
                              <article>
                                <header>
                                    <img class="background-image" alt="Developer" src="img/Osama.jpg">
                                    Osama Al-Najjar<br>
                                    <small>Big Boss and all</small>
                                </header>
                                <footer>
                                    <h4>
                                        Osama Al-Najjar
                                    </h4>
                             He is just yelling, ordering and sitting around while playing with his belly button.
                                    <br>
                                    <br>
                                        <div class="hover-state text-center preserve3d">
                                            <div class="social-links ">
                                                <a href="#"><i class="icon social_twitter"></i></a>
                                                <a href="#"><i class="icon social_linkedin"></i></a>
                                            </div>
                                        </div>
                                </footer>
                              </article>
                            </li>


                            <li>
                              <article>
                                <header>
                                    <img class="background-image" alt="Developer" src="img/Sultan.jpg">
                                    Sultan Al-Khayal<br>
                                    <small>The Awesome CronJobber</small>
                                </header>
                                <footer>
                                    <h4>
                                        Sultan Al-Khayal
                                    </h4>
                             Sultan can really get any job you give to him accomblished, masha' Allah. He Awesome, awesome and AWESOME!!
                                   <br>
                                    <br>
                                        <div class="hover-state text-center preserve3d">
                                            <div class="social-links ">
                                                <a href="#"><i class="icon social_twitter"></i></a>
                                                <a href="#"><i class="icon social_linkedin"></i></a>
                                            </div>
                                        </div>
                                </footer>
                              </article>
                            </li>
                        </ul>
                    </div>
                  </div>
                </div>




                    </div><!--end of container-->

                </section>

                <a id="schedule" class="in-page-link"></a>

                <a id="contact" class="in-page-link"></a>



                <a id="subscribe" class="in-page-link"></a>


            </div>
            <div class="footer-container">			
                <footer class="classic">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-3">
                            </div>

                            <div class="col-sm-5">
                                <span class="lead color-heading">About SectionMe</span>
                                <p>
                                    SectionMe is created to help students registering courses, it was inspired by the pressure and depression of the Registration Week. So, we created a tool that releafs you from                                         refreshing the RegWeb page every 10 seconds to see whether it closed or open.
                                </p>
                            </div>

                            <div class="col-sm-4">
                                <ul class="contact-methods">
                                    <li><i class="icon pe-7s-mail"></i><span>info@sectionMe.com</span></li>
                                    <li><i class="icon pe-7s-map-marker"></i><span>KFUPM, Dhahran</span></li>
                                </ul>
                            </div>
                        </div><!--end of row-->


                    </div><!--end of container-->
                </footer>
            </div>



            <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ></script>

            <script src="js/bootstrap.min.js"></script>
            <script src="js/skrollr.min.js"></script>
            <script src="js/spectragram.min.js"></script>
            <script src="js/flexslider.min.js"></script>
            <script src="js/jquery.plugin.min.js"></script>
            <script src="js/jquery.countdown.min.js"></script>
            <script src="js/lightbox.min.js"></script>
            <script src="js/smooth-scroll.min.js"></script>
            <script src="js/twitterfetcher.min.js"></script>
            <script src="js/scripts.js"></script>
            <script src="countdown.js"></script>


            <script type="text/javascript">
    $("document").ready(function(){
        $(".email-subscribe").submit(function(){

            var data = {
                "action": "test"
            };
            data = $(this).serialize() + "&" + $.param(data);
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "getCrnCourse.php", //Relative or absolute path to response.php file
                data: data,
                //beforeSend:function(){
                //	var l = $( '.btn' ).ladda();

                // Start loading
                //l.ladda( 'start' );

                //},
                success: function(data) {

                    //$('.cd-form').removeClass('is-submitted').find('.cd-loading').off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend');
                    var courseStatus = data["status"];
                    var courseName = data["course"];

                    alert(courseStatus);

                    if(courseStatus.indexOf("CLOSE") >= 0 )
                    {
                        // Save course name and other stuff to hidden id input.
                        //$('.cd-response-success').addClass('slide-in');
                        //$('#course').html(courseName);
                    } else {
                        //$('.cd-response-error').addClass('is-visible');
                    }
                    //OPEN    &nbsp;
                    //alert(courseStatus);
                    //alert(courseName); fghtnroigndfljnfoubn
                },
                error: function() {
                    alert("error");
                    //$('.cd-form').removeClass('is-submitted').find('.cd-loading').off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend');
                    //$('.cd-response-notification').addClass('is-visible');
                }
            });
            return false;
        });
    });

            </script>
        </body>
    </html>
